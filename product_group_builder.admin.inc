<?php

/**
 * @file
 * Administrative things for product group builder.
 */

/**
 * Admin form for product group builder.
 */
function product_group_builder_admin_form() {
  $form = array();
  $form[PRODUCT_GROUP_BUILDER_CONTENT_TYPE_VARIABLE] = array(
    // @todo: Make a select field.
    '#type' => 'textfield',
    '#default_value' => variable_get(PRODUCT_GROUP_BUILDER_CONTENT_TYPE_VARIABLE, 'product_group'),
    '#title' => t('Node type to use for product display'),
  );
  $form[PRODUCT_GROUP_BUILDER_FIELD_NAME_VARIABLE] = array(
    // @todo: Make a select field. Preferrably ajax based on content type.
    '#type' => 'textfield',
    '#default_value' => variable_get(PRODUCT_GROUP_BUILDER_FIELD_NAME_VARIABLE, 'field_product'),
    '#title' => t('Field name in the node type'),
  );
  return system_settings_form($form);
}

/**
 * Menu callback for the creating of product groups.
 */
function product_group_builder_add_form() {
  $form = array();
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
  );
  $i = 0;
  $form['references'] = array(
    '#type' => 'fieldset',
    '#title' => t('Products in the group'),
    '#description' => t('If you dont need all of these rows, just leave some of them blank.'),
    '#tree' => TRUE,
  );
  while ($i < 10) {
    $i++;
    $form['references'][] = array(
      '#type' => 'entityreference',
      '#title' => t('Product reference'),
      '#era_entity_type' => 'commerce_product',
      '#era_bundles' => array(
        'product',
      ),
      '#era_query_settings' => array(
        'limit' => 15,
        'property_conditions' => array(
          array('status', 1),
        ),
      ),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create group'),
  );
  return $form;
}

/**
 * Submitter function for this thing.
 */
function product_group_builder_add_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $products = array();
  foreach ($values['references'] as $reference) {
    // Value will be FALSE if the field was left empty.
    if ($reference) {
      // Just double check that it exists and is active.
      $id = $reference['entity_id'];
      if (!$product = commerce_product_load($id)) {
        continue;
      }
      if (!$product->status) {
        continue;
      }
      $products[] = $product;
    }
  }
  try {
    $bundle_products = array();
    global $user;
    // First create the bundle items.
    foreach ($products as $product) {
      /** @var \Entity $item */
      $bundle_item_values = array(
        'type' => 'bundle_item',
        'status' => 1,
        'commerce_bundle_product' => array(
          LANGUAGE_NONE => array(
            array('target_id' => $product->product_id),
          ),
        ),
      );
      drupal_alter('product_group_builder_bundle_item_values', $bundle_item_values, $values);
      $item = entity_create('commerce_bundle_item', $bundle_item_values);
      $item->save();
      // Then create the product bundle product.
      $bundle_product_values = array(
        'type' => 'commerce_bundle_group',
        'title' => t('Bundle group for @product', array(
          '@product' => $product->title,
        )),
        'language' => LANGUAGE_NONE,
        'commerce_bundle_items' => array(
          LANGUAGE_NONE => array(
            array('target_id' => $item->identifier()),
          ),
        ),
        'sku' => sprintf('bundle_%d_%s', $item->identifier(), $product->sku),
        'commerce_bundle_unit_quantity' => array(
          LANGUAGE_NONE => array(
            array('value' => 1),
          ),
        ),
        'status' => 1,
      );
      drupal_alter('product_group_builder_bundle_product_values', $bundle_product_values, $values);
      $bundle_product = entity_create('commerce_product', $bundle_product_values);
      commerce_product_save($bundle_product);
      $bundle_products[] = $bundle_product;
    }
    // Then create the node.
    $field_name = variable_get(PRODUCT_GROUP_BUILDER_FIELD_NAME_VARIABLE, 'field_product');
    $product_field_data = array();
    foreach ($bundle_products as $bundle_product) {
      $product_field_data[] = array('product_id' => $bundle_product->product_id);
    }
    $node_values = array(
      'type' => variable_get(PRODUCT_GROUP_BUILDER_CONTENT_TYPE_VARIABLE, 'product_group'),
      'uid' => $user->uid,
      'title' => $values['title'],
      $field_name => array(
        LANGUAGE_NONE => $product_field_data,
      ),
      'status' => NODE_PUBLISHED,
    );
    drupal_alter('product_group_builder_node_values', $bundle_product_values, $values);
    $node = entity_create('node', $node_values);
    node_save($node);
    $form_state['redirect'] = 'node/' . $node->nid . '/edit';
  }
  catch (Exception $e) {
    watchdog_exception(PRODUCT_GROUP_BUILDER_LOG_TAG, $e);
  }
}
